# minecraft-docker


This is a Minecraft-Docker with default running Paperspigot 1.20.2 - change the download path in the Dockerfile to have another version installed.

When deleting the volume, the world gets deleted so make sure it stays persistent.